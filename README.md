## hathi-client

### Purpose
The hathi-client is intended for testing the hathi-server.

### Status
The current version of the client is a python client and server pair. The python server echoes whatever the python client sends. The python client will eventually be used to test the hathi-server. For now, it works in parallel with the python server.  

There is also hathi-mock which... [TODO]

And we just added testactivipy.py which shows how activipy can store and fetch activity stream objects.

### Dependencies
Your python client may not immediately work if you are missing the following modules, please install `pip3 install _` to be able to run hathi-client.

activipy

### Use
Using CLI

```
git clone `copy from hathi-client` hathi-client
cd hathi-client
python3 server.py
```
open another shell window

```
cd hathi-client
python3 client.py
```

Client and server both have rough argparse to explain the few choices available in the strawman versions

### Hathi-mock passive server mode
Running hathi-mock.py -s can be used for developing a Hathi client implementation. To get a sample data, make a GET request using the following URL: http://<server>:<port>/<ID>

Where:

  * server and port = Server and port where `hathi-mock.py -s` is listening on
  * ID = path to use to get the requested item:
    * 0 = A note
    * 1 = The Create object that created the Note object above
    * tester = An Actor object
    * tester/inbox = "tester" Actor's inbox collection
    * tester/outbox = "tester" Actor's outbox collection
    * tester/following = "tester" Actor's following collection
    * tester/followers = "tester" Actor's followers
