#!/usr/bin/env python
# # -*- coding: utf-8 -*-

"""client.py
created as strawman

connects to a server, sends a message, and repeats what the server returns
"""

import sys
import argparse
import hathiLib
import cmd
import tempfile
import os
from subprocess import call


def callEditor(initial):
    # Code ripped from stackoverflow.com/questions/6309587/
    EDITOR = os.environ.get("EDITOR", "nano")

    with tempfile.NamedTemporaryFile(suffix=".tmp") as tf:
        tf.write(initial)
        tf.flush()
        call([EDITOR, tf.name])
        
        # do the parsing with `tf` using regular File operations.
        # for instance:
        tf.seek(0)
        edited = tf.read()
        return edited

class Hathiq(cmd.Cmd):
    def __init__(self, actorID):
        cmd.Cmd.__init__(self)
        self.prompt = "[] "
        self.actorID = actorID
        self.gotBoxes = False
        self.currentBox = "out"

    def getBoxIDs(self):
        if self.gotBoxes is False:
            actor = hathiLib.getObject(self.actorID)
            self.outboxID = actor["outbox"]
            self.inboxID = actor["inbox"]
            self.followersID = actor["followers"]
            self.followingID = actor["following"]
            self.gotBoxes = True

    def getCurrentBox(self):
        boxes = {"in": self.inboxID, "out": self.outboxID,
                 "folw": self.followersID, "folg": self.followingID}
        boxID = boxes[self.currentBox]
        box = hathiLib.getObject(boxID)
        return box

    def emptyline(self):
        pass

    def do_exit(self, line):
        return True

    do_quit = do_exit

    def do_inbox(self, line):
        self.currentBox = "in"
        self.prompt = "[inbox] "

    def do_outbox(self, line):
        self.currentBox = "out"
        self.prompt = "[outbox] "

    def do_followers(self, line):
        self.currentBox = "folw"
        self.prompt = "[followers] "

    def do_following(self, line):
        self.currentBox = "folg"
        self.prompt = "[following] "

    def do_show(self, line):
        self.getBoxIDs()
        if line != "":
            try:
                line = int(line)
                box = self.getCurrentBox()
                try:
                    items = box["items"]
                except KeyError:
                    print("Empty")
                    return
                if isinstance(items, (list, tuple)) is False:
                    items = [items]
                index = len(items) - line
                if index >= 0:
                    obj = hathiLib.getObject(items[index])
                    if obj["@type"] == "Note":
                        displayNote(obj)
                    else:
                        displayUnknown(obj)
                else:
                    print("Invalid box index")
            except ValueError:
                obj = hathiLib.getObject(line)
                if obj["@type"] == "Note":
                    displayNote(obj)
                else:
                    displayUnknown(obj)
        else:
            box = self.getCurrentBox()
            displayBox(box, 5)

    def do_actor(self, line):
        line = line.split()
        lineLen = len(line)
        if lineLen == 1:  # Switch to actor
            self.actorID = line[0]
            self.gotBoxes = False
        elif (lineLen == 3) and ("-c" == line[0]):  # Create actor
            hathiLib.createActor(line[1], line[2])

    def do_note(self, line):
        self.getBoxIDs()
        initial = b"""#Addresses may be seperated by commas.
#To:
#Cc:
#Bto:
#Bcc:
#Summary:
#Body:

"""
        edited = callEditor(initial).decode("utf8")
        if edited == initial.decode("utf8"):
            return
        hitBody = False
        to = []
        cc = []
        bto = []
        bcc = []
        summary = ""
        bodyLines = []
        for line in edited.splitlines():
            if hitBody is False:
                if line.startswith("#To:"):
                    to += processAddressLine(line)
                elif line.startswith("#Cc:"):
                    cc += processAddressLine(line)
                elif line.startswith("#Bto:"):
                    bto += processAddressLine(line)
                elif line.startswith("#Bcc:"):
                    bcc += processAddressLine(line)
                elif line.startswith("#Summary:"):
                    summary = line.split(":", maxsplit=1)[1].strip()
                elif line.startswith("#Body:"):
                    hitBody = True
                    bodyLines.append(line.split(":", maxsplit=1)[1].lstrip())
            else:
                bodyLines.append(line)
        body = "\n".join(bodyLines)
        print("to:", to)
        print("cc:", cc)
        print("bto:", bto)
        print("bcc:", bcc)
        print("summary:", summary)
        print("body:", body)
        location, respBody = hathiLib.createNote(self.outboxID,
                                                 to, cc, bto, bcc,
                                                 summary, body)

def processAddressLine(line):
    line = line.split(":", maxsplit=1)[1]
    line = line.split(",")
    addrs = []
    for addr in line:
        addrs.append(addr.strip())
    return addrs

def displayBox(box, lineCount):
    try:
        items = box["items"]
    except KeyError:
        print("Empty")
        return
    if isinstance(items, tuple) is True:
        items = list(items)
    elif isinstance(items, list) is False:
        items = [items]
    else:
        items = items[:]
    if len(items) > lineCount:  # Get a subset of lines
        items = items[-lineCount:]
    fmt = "%i | %s | %s"
    i = 1
    items.reverse()
    for itemID in items:
        itemObj = hathiLib.getObject(itemID)
        s = fmt % (i, itemObj["@type"], itemID)
        print(s)
        i += 1

def tryDisplayField(obj, fieldName, prefix=""):
    try:
        value = obj[fieldName]
    except:
        return
    if isinstance(value, dict):
        if "en" in value.keys():  # TODO: prefered lang option
            print(prefix + value["en"])
        else:  # Just dump them all
            for k, v in value.items():
                print(prefix + k + ": " + v)
    else:
        print(prefix + value)

def displayNote(note):
    tryDisplayField(note, "@id")
    tryDisplayField(note, "name")
    tryDisplayField(note, "summary", "Summary: ")
    tryDisplayField(note, "to", "To: ")
    tryDisplayField(note, "cc", "Cc: ")
    tryDisplayField(note, "bto", "Bto: ")
    tryDisplayField(note, "bcc", "Bcc: ")
    print("=" * 40)
    tryDisplayField(note, "content")
    print()

def displayUnknown(obj):
    for k, v in obj.json().items():
        print("%s: %s" % (k, v))


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='A simple message client',
                                     epilog="run python3 server.py before "
                                     "running the client in another shell")
    parser.add_argument('-host', default='127.0.0.1',
                        help='a specific host if not the localhost')
    parser.add_argument('-message', type=str, default="message",
                        help='a specific port (default: message)')
    parser.add_argument("-a", "--actor", action="store", dest="actorID",
                        help="The Actor ID to use")
    args = parser.parse_args()

    interpreter = Hathiq(args.actorID)
    interpreter.cmdloop()
