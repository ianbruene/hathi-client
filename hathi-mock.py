#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse
import http.client
from http.server import BaseHTTPRequestHandler, HTTPServer
import json
import urllib.parse
import testing.mockDB
import testing.util as util
import testing.sequences
from testing.variables import *
from activipy import core
import hathi.validation

# Welcome to hathi-mock
# ----------------------------
# The "hathi-mock" program is on the surface nothing more than a testing
# tool. But as a "testing tool" it also provides several other functions.
# Those functions include:
#
#  * Coordination between client and server implementations of the protocol
#  * The compliance standard for 3rd-party software that wishes to interact
#    with the Hathi network.
#  * Documentation of the protocol
#
# That last item is actually the most important one when it comes to
# commenting; every test sequence and function for running the sequences is
# extensively commented so that even someone completely unfamiliar with the
# Hathi codebase can learn from reading it.
#
# If this file does not properly document what *should be* then it is
# considered a bug.
#
# If another part of the codebase does not respond properly to what this
# document correctly says should be, that part of the codebase has a bug.

# ===================================================================
# BE ADVISED: this file is still in high flux. Parts are not fully
# documented yet. We are working on that.
# ===================================================================
#
# The test sequences themselves are in the file testing/sequences.py

verbosity = 0


vars = testing.sequences.vars

c2s_sequences = testing.sequences.c2s_sequences

# Timestamps will be for the time-of-running, and as such cannot be predicted
# in advance.
timeFields = ("deleted",)


def prepareMessage(msgDict):
    prepped = {}
    for key, value in msgDict.items():
        if isinstance(value, VariableNode):
            value = value.get()
        elif isinstance(value, dict):
            value = prepareMessage(value)
        prepped[key] = value
    return prepped


def prepareValue(value):
    if isinstance(value, VariableNode):
        value = value.get()
    return value


def send_client_request(addr, port, node):
    header = {}
    # Most communication between Hathi software takes place using JSON strings,
    # however this code has been left flexible so that if raw text is needed
    # in the future there won't be as many things to change.
    if isinstance(node["data"], dict):
        # Encode the data in JSON format
        data = prepareMessage(node["data"])
        body = json.dumps(data)
        header["Content-Type"] = "application/json"
    else:
        body = node["data"]
        header["Content-Type"] = "text/plain"
    # Contact the server, send our request, and get the result
    conn = http.client.HTTPConnection(addr, port)
    conn.request(node["action"], prepareValue(node["path"]), body, header)
    resp = conn.getresponse()
    respBody = resp.read()
    conn.close()
    return resp, respBody


def compareMessage(target, testee):
    if isinstance(target, VariableNode):  # Leaf we need to mung
        if target.isSet():  # Setting value, no comparing to do
            target.set(testee)
            return True
        else:
            return target.get() == testee
    elif isinstance(target, (list, tuple)):
        tarLen = len(target)
        if isinstance(testee, (list, tuple)) is False:
            return False
        elif tarLen != len(testee):
            return False
        else:
            for i in range(tarLen):
                if compareMessage(target[i], testee[i]) is False:
                    return False
    elif isinstance(target, dict):
        if isinstance(testee, dict) is False:
            return False
        elif len(target) != len(testee):
            return False
        else:
            for key, value in target.items():
                if key in timeFields:
                    continue
                if compareMessage(value, testee[key]) is False:
                    return False
    else:
        return target == testee
    return True


def execute_client_request(addr, port, node):
    resp, respBody = send_client_request(addr, port, node)
    # If the server returned a JSON string then convert it to a dict tree
    contentType = resp.getheader("Content-Type")
    if contentType and ("application/json" in contentType):
        respBody = json.loads(respBody)
    # We need to make sure the response from the server is correct
    # Check Location header
    location = resp.getheader("Location")
    locationMatch = compareMessage(node.get("returnLocation"), location)
    if locationMatch is False:
        expected = node.get("returnLocation")
        if isinstance(expected, VariableNode):
            expected = expected.get()
        return "%s ERROR: got location %s, expected %s" % \
            (node["testName"], repr(location), repr(expected))
    # Check status code
    if (resp.status != node["returnCode"]):
        return "%s ERROR: got status %s, expected status %s" \
            % (node["testName"], resp.status, node["returnCode"])
    # Check body
    respMatch = compareMessage(node["returnData"], respBody)
    if respMatch is False:
        return "%s ERROR: got %s return data, expected %s" % \
            (node["testName"], repr(respBody), repr(node["returnData"]))
    else:
        # None means we got what was expected for this test.
        return None


def do_client(addr, port):
    vars.setServerData(addr, port)
    for node in c2s_sequences:
        if verbosity >= 1:
            path = node["path"]
            if isinstance(path, VariableNode):
                path = path.get()
                print("%s: %s %s %s\t" % (node["testName"],
                                          addr, port, path),
                      end="")
        result = execute_client_request(addr, port, node)
        if result is None:
            if verbosity >= 1:
                print(".....ok")
        else:
            print("\n", result)


def do_a_server(addr, port):
    print("Active server mode not yet implemented")


class MockHathiServer(BaseHTTPRequestHandler):
    def _loaddb(self, body):
        try:
            payload = json.loads(body)

            # Make sure all references to the object mockdb references are
            # looking at the latest version of the DB
            mockdb.clear()
            for i in payload["data"]:
                try:
                    oid = i["@id"]
                    mockdb[oid] = core.ASObj(i)
                except KeyError:
                    print("mockdb object missing @id:", i)

            print("New mockdb: %s" % (mockdb))

            self.send_response(200)
            self.send_header("Content-type", "text/text")
            self.end_headers()
            self.wfile.write(bytes("DB loaded", "utf8"))
        except json.JSONDecodeError:
            print("Failed to parse DB to load")
            self.send_response(400)
            self.end_headers()
            return

    def do_GET(self):
        if verbosity >= 1:
            print("\n--- GET begin ---\n")
            print(self.raw_requestline)
            print(self.headers)
            print("\n--- GET end ---\n")
        parsedpath = urllib.parse.urlparse(self.path)
        ID = util.makeIDFromPath(address[0], address[1], parsedpath.path)
        if ID in mockdb.keys():
            obj = mockdb[ID]
            body = json.dumps(obj.json())
            self.send_response(200)
            self.send_header("Content-type", "application/json")
            self.end_headers()
            self.wfile.write(bytes(body, "utf8"))
        else:
            self.send_response(404)
            self.end_headers()

    def do_POST(self):
        try:
            parsedpath = urllib.parse.urlparse(self.path)
            content_len = int(self.headers['content-length'])
            body = self.rfile.read(content_len)
            if verbosity >= 1:
                print("\n---POST begin ---\n")
                print(self.raw_requestline)
                print(self.headers)
                print(body)
                print("\n--- POST end --- \n")

            if parsedpath.path == "/loaddb":
                self._loaddb(body)
                return

            # ID = util.makeIDFromPath(address[0], address[1], parsedpath.path)
            try:
                jsonified = json.loads(body)
            except json.decoder.JSONDecodeError:
                print("Failed to json-decode message")
                self.send_response(400)
                return
            # Validate and report on the results
            res = hathi.validation.validatePOSTObject(jsonified, self.headers)
            valid, reports, aliases, specials = res
            print("POST validation reports:")
            for report in reports:
                print("\t" + report)
            print("Aliases:")
            for item in aliases.items():
                print("%s: %s" % item)
            print("Special fields:")
            for item in specials.items():
                print("%s: %s" % item)
            print("End POST report")
            if valid is False:
                self.send_response(415)
                self.end_headers()
                return
            # Decide on acceptable POSTs (aka: Actors?)
            self.send_response(201)
            self.send_header("Location",
                             util.makeIDFromPath(address[0], address[1],
                                                 "PlaceholderID"))
            self.end_headers()
        except Exception as e:
            self.send_response(500)
            self.end_headers()
            raise e


def do_p_server(addr, port):
    global mockdb
    mockdb = testing.mockDB.buildStartingDB(addr, port)
    global address
    address = (addr, port)
    server = HTTPServer((addr, port), MockHathiServer)
    server.serve_forever()


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-c", "--mock-client", action="store_const",
                        dest="mode", const="client",
                        help="Pose as a client to test the server at "
                        "the specified address and port number")
    parser.add_argument("-s", "--mock-passive-server", action="store_const",
                        dest="mode", const="passive-server",
                        help="Pose as a server on the specified port number "
                        "and await connections from clients or other servers")
    parser.add_argument("-S", "--mock-active-server", action="store_const",
                        dest="mode", const="active-server",
                        help="Pose as a server to test the server at the "
                        "specified address and port number")
    parser.add_argument("-a", "--address", action="store", dest="address",
                        default="localhost",
                        help="Address of server to connect to for testing")
    parser.add_argument("-p", "--port", action="store", dest="port", type=int,
                        default=8080,
                        help="Port number of server to connect to, or if "
                        "listening for connections port number to listen on")
    parser.add_argument("-v", "--verbose", action="count", dest="verbosity")
    args = parser.parse_args()
    if args.mode is None:
        parser.print_help()
        raise SystemExit(1)
    if args.verbosity is not None:
        verbosity = args.verbosity
    modes = {"client": do_client,
             "active-server": do_a_server,
             "passive-server": do_p_server}
    modes[args.mode](args.address, args.port)
