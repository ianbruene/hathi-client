#!/usr/bin/env python
# -*- coding: utf-8 -*-

import unittest
import validation

class TestValidation(unittest.TestCase):
    def test_validateContentType(self):
        f = validation.validateContentType

        # Test correct
        self.assertEqual(f('application/ld+json; ' \
                           'profile="https://www.w3.org/ns/activitystreams"'),
                         (True, True))
        # Test acceptable alternative
        self.assertEqual(f("application/activity+json"), (False, True))
        # Test wrong
        self.assertEqual(f("foo"), (False, False))

    def test_buildAliases(self):
        f = validation.buildAliases

        # Test no @context
        self.assertEqual(f({"foo": 23, "bar": 42}), ({}, {}, True))
        # Test string @context, correct
        self.assertEqual(f({"@context":
                            "https://www.w3.org/ns/activitystreams"}),
                         ({},
                          {"@vocab": "https://www.w3.org/ns/activitystreams"},
                          True))
        # Test string @context, correct alt
        self.assertEqual(f({"@context":
                            "http://www.w3.org/ns/activitystreams"}),
                         ({},
                          {"@vocab": "http://www.w3.org/ns/activitystreams"},
                          True))
        # Test string @context, incorrect
        self.assertEqual(f({"@context": "foo"}),
                         ({}, {"@vocab": "foo"}, False))
        # Test dict @context
        self.assertEqual(f({
            "@context": {
                "@vocab": "https://www.w3.org/ns/activitystreams",
                "@language": "en",
                "foo": "23"
                }}),
                         ({"foo": "23"},
                         {"@vocab": "https://www.w3.org/ns/activitystreams",
                          "@language": "en"},
                          True))
        # Test list @context
        self.assertEqual(f({ "@context": [
            "https://www.w3.org/ns/activitystreams",
            {"foo": "baz"}]}),
                         ({"foo": "baz"},
                         {"@vocab": "https://www.w3.org/ns/activitystreams"},
                          True))

    def test_isKnownToken(self):
        f = validation.isKnownToken

        valids = ["quux", "bar"]
        aliases = {"foo": "Fooblarghium",
                   "baz": "Bazonite"}
        # Test bare field, unknown
        self.assertEqual(f("one", aliases, valids), False)
        # Test bare field, known
        self.assertEqual(f("bar", aliases, valids), True)
        # Test as-prefixed field, unknown
        self.assertEqual(f("as:one", aliases, valids), False)
        # Test as-prefixed field, known
        self.assertEqual(f("as:bar", aliases, valids), True)
        # Aliased name
        self.assertEqual(f("foo:Foo!", aliases, valids), "Fooblarghium")

    def test_validateMappableField(self):
        f = validation.validateMappableField

        # Test string-form, correct
        self.assertEqual(f("summary", "foo"), True)
        # Test string-form, incorrect
        self.assertEqual(f("summary", {"en": "foo"}), False)
        # Test dict-form, correct
        self.assertEqual(f("summaryMap", {"en": "foo"}), True)
        # Test dict-form, incorrect
        self.assertEqual(f("summaryMap", "foo"), False)
        # Test unknown field
        self.assertEqual(f("something", "bar"), None)

    def test_trawlObejct(self):
        f = validation.trawlObject

        aliases = {"foo": "Foolargia"}
        # Perfect object, simple
        perfectSimpObj = {"@context": {"foo": "Foolargia"}, "@type": "Create",
                          "summary": "foo", "icon": "foo.png",
                          "object": {"@type": "Note",
                                     "content": "arghle blarghl"}}
        # Perfect object, complex
        perfectCompObj = {"@context": {"foo": "Foolargia", "@language": "en"},
                          "@type": ["Create", "foo:Bandersnatch"],
                          "summaryMap": {"en": "foo", "und": "arrrgh"},
                          "as:object": {"@type": "foo:Jabberwocky",
                                     "foo:slithy": "tove"}}
        # Imperfect object, simple
        imperfectSimpObj = {"@context": {"foo": "Foolargia"}, "type": "Void",
                            "summaryMap": "hello!", "staring": "back"}
        # Imperfect object, complex
        imperfectCompObj = {"@context": {"foo": "Foolargia",
                                         "@language": "en"},
                            "@type": ["Void", "bar:Bandersnatch"],
                            "summary": {"en": "foo", "und": "arrrgh"},
                            "staring": {"@type": "bar:Jabberwocky",
                                        "bar:slithy": "tove"}}
        # Test perfect simple
        self.assertEqual(f(perfectSimpObj, aliases), [])
        # Test perfect complex
        self.assertEqual(f(perfectCompObj, aliases), [])
        # Test imperfect simple
        self.assertEqual(f(imperfectSimpObj, aliases),
                         ["Alias used: type",
                          "Unknown type: Void",
                          "Misused field summaryMap",
                          "Unknown field: staring"])
        # Test imperfect complex
        self.assertEqual(f(imperfectCompObj, aliases),
                         ["Unknown type: Void",
                          "Unknown type: bar:Bandersnatch",
                          "Misused field summary",
                          "Unknown field: staring",
                          "Unknown type: bar:Jabberwocky",
                          "Unknown field: bar:slithy"])

    def test_validateGETObject(self):
        f = validation.validateGETObject

        goodHeaders = {"Content-type": 'application/ld+json; ' \
                       'profile="https://www.w3.org/ns/activitystreams"'}
        okHeaders = {"Content-type": "application/activity+json"}
        badHeaders = {"Content-type": "foo"}
        # String @context
        strObj = {"@context": "https://www.w3.org/ns/activitystreams",
                  "@type": "Create", "icon": "foo.png"}
        # Object @context
        objObj = {
            "@context": {
                "foo": "Foolargia",
                "@language": "en",
                "@vocab": "https://www.w3.org/ns/activitystreams"},
            "@type": "Create", "icon": "foo.png"}
        # List @context
        listObj = {"@context": ["https://www.w3.org/ns/activitystreams",
                                {"foo": "Foolargia"}],
                   "@type": "Create", "icon": "foo.png"}
        # Empty @context
        emptyObj = {"@type": "Create", "icon": "foo.png"}
        # Empty @context/@vocab
        evocabObj = {"@context": {"foo": "Foolargia"}, "@type": "Create",
                     "icon": "foo.png"}
        # Typeless type
        typelessObj = {"@context": "https://www.w3.org/ns/activitystreams",
                       "icon": "foo.png"}
        # Test string @context
        self.assertEqual(f(strObj, goodHeaders),
                         (True, [], {},
                          {"@vocab": "https://www.w3.org/ns/activitystreams"}))
        # Test obj @context
        self.assertEqual(f(objObj, goodHeaders),
                         (True, [], {"foo": "Foolargia"},
                          {"@vocab": "https://www.w3.org/ns/activitystreams",
                           "@language": "en"}))
        # Test list @context
        self.assertEqual(f(listObj, goodHeaders),
                         (True, [], {"foo": "Foolargia"},
                          {"@vocab": "https://www.w3.org/ns/activitystreams"}))
        # Test empty @context
        self.assertEqual(f(emptyObj, goodHeaders),
                         (True, ["Empty @context or @context/@vocab"], {}, {}))
        # Test empty @context/@vocab
        self.assertEqual(f(evocabObj, goodHeaders),
                         (True, ["Empty @context or @context/@vocab"],
                          {"foo": "Foolargia"}, {}))
        # Test typeless type
        self.assertEqual(f(typelessObj, goodHeaders),
                         (False, ["Object has no @type"], {}, {}))
        # Test alternate Content-type
        self.assertEqual(f(strObj, okHeaders),
                         (True, ["Content-type using alternate type"], {},
                          {"@vocab": "https://www.w3.org/ns/activitystreams"}))
        # Test bad Content-type
        self.assertEqual(f(strObj, badHeaders),
                         (False, ["Incorrect Content-type"], {}, {}))

    def test_validatePOSTObject(self):
        f = validation.validatePOSTObject

        goodHeaders = {"Content-type": 'application/ld+json; ' \
                       'profile="https://www.w3.org/ns/activitystreams"'}
        noIDObj = {"@context": "https://www.w3.org/ns/activitystreams",
                   "@type": "Create", "icon": "foo.png"}
        idObj = {"@context": "https://www.w3.org/ns/activitystreams",
                 "@type": "Create", "icon": "foo.png", "@id": "mu"}

        # Test object without ID
        self.assertEqual(f(noIDObj, goodHeaders),
                         (True, [], {},
                          {"@vocab": "https://www.w3.org/ns/activitystreams"}))
        # Test object without ID
        self.assertEqual(f(idObj, goodHeaders),
                         (True, ["ID field present in POST"], {},
                          {"@vocab": "https://www.w3.org/ns/activitystreams"}))


if __name__ == "__main__":
    unittest.main()
