#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from activipy import vocab, core

def buildStartingDB(hostname, port):
    db = {}
    addrRoot = "http://%s:%i/" % (hostname, port)
    noteID = addrRoot + "0"
    db[noteID] = vocab.Note(id=noteID, summary="Foo bar", content="Foo bar baz quux!")
    createID = addrRoot + "1"
    db[createID] = vocab.Create(id=createID, object=noteID)
    inboxID = addrRoot + "tester/inbox"
    db[inboxID] = vocab.OrderedCollection(id=inboxID)
    outboxID = addrRoot + "tester/outbox"
    db[outboxID] = vocab.OrderedCollection(id=outboxID)
    followingID = addrRoot + "tester/following"
    db[followingID] = vocab.OrderedCollection(id=followingID)
    followersID = addrRoot + "tester/followers"
    db[followersID] = vocab.OrderedCollection(id=followersID)
    personID = addrRoot + "tester"
    db[personID] = vocab.Person(id=personID, displayName="Foo", inbox=inboxID,
                                outbox=outboxID, following=followingID,
                                followers=followersID)
    return db
