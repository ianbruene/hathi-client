#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from testing.variables import *

vars = VariableDatabase()

# Test Sequences
# [description of test sequence formatting here]
# [still high-flux]

# Actor Creation
# --------------
# "Actor"s are what represent people, organizations, and other active
# entities. They are also the focal point through which all activity happens.
# To create an Actor the client POSTs an Actor object. The target of the POST
# is the outbox of what will be the new Actor, in this case "testing/outbox".
#
# The full list of Actor types is:
#   * Person
#   * Organization
#   * Service
#   * Group
#   * Application
#
# Upon sucessful creation of the Actor the server will respond with HTTP
# status code 201 Created. It will also return a JSON string with the ID of
# the Actor.

createActor = {"testName": "Create Actor 1",
               "path": vars.newIDPath("act1Out", "testing/outbox"),
               "action": "POST",
               "data": {"@type": "Person", "name": "Testman"},
               "returnCode": 201, "returnData": b"",
               "returnLocation": vars.newVar("act1")}

createActor2 = {"testName": "Create Actor 2",
                "path": vars.newIDPath("act2Out", "testing2/outbox"),
                "action": "POST",
                "data": {"@type": "Person", "name": "Testcat"},
                "returnCode": 201, "returnData": b"",
                "returnLocation": vars.newVar("act2")}

# Retrieving an object
# --------------------
# Objects are retrieved from a server by making an HTTP GET request.
# If the object if public or the client presents valid authentication for
# the object then it will be returned as a JSON string.
#
# In this case we are retrieving the Actor that was created earlier.
#
# Note that part of the return data includes the "altitude" field. This is
# due to all Hathi objects having this field defined, but there not being
# an obvious null value at this time. This is a low-priority bug that will be
# fixed in time.
#
# Also note that in the previous test the "name" field was a simple string,
# but here it is a dict with the key "en". The "name", "content", and "summary"
# fields are actually language maps which contain different data for different
# languages. If a language map is provided with a bare string it is given the
# assumed language code of "en" for English.

getActor1 = {"testName": "Get created Actor 1", "path": vars.usePath("act1"),
             "action": "GET", "data": None, "returnCode": 200,
             "returnData": {"@id": vars.useID("act1"),
                            "@type": "Person", "name": "Testman",
                            "inbox": vars.newID("act1In", "testing/inbox"),
                            "outbox": vars.useID("act1Out"),
                            "following": vars.newID("act1Folg",
                                                    "testing/following"),
                            "followers": vars.newID("act1Folw",
                                                    "testing/followers")}}

getActor2 = {"testName": "Get created Actor 2", "path": vars.usePath("act2"),
             "action": "GET", "data": None, "returnCode": 200,
             "returnData": {"@id": vars.useID("act2"),
                            "@type": "Person", "name": "Testcat",
                            "inbox": vars.newID("act2In", "testing2/inbox"),
                            "outbox": vars.useID("act2Out"),
                            "following": vars.newID("act2Folg",
                                                    "testing2/following"),
                            "followers": vars.newID("act2Folw",
                                                    "testing2/followers")}}

# Creating a Note
# ---------------
# Any time the user wishes to do something with an Actor the client will POST
# an Activity object to the Actor's outbox. The server will then propogate
# the activity to any other Actor's inboxes as necessary.
#
# In this case we are POSTing a Create Activity containing a Note object.
# Only Activities are supposed to be posted to the outbox, however if a
# different type of object is posted to it the server will generate a Create
# Activity and wrap the object in it before proceeding normally.

createSimple = {"testName": "Simple Create Note",
                "path": vars.usePath("act1Out"), "action": "POST",
                "data": {"@type": "Create",
                         "object": {"@type": "Note", "content": "blah blah",
                                    "summary": "foo!"},
                         "to": vars.useID("act2")},
                "returnCode": 201, "returnData": b"",
                "returnLocation": vars.newVar("createNote1")}

# Getting an Outbox
# -----------------
# Every Actor has several defined Collection / OrderedCollection objects,
# among them are:
#
# * Outbox
# * Inbox
# * Following
# * Followers
#
# The correct method of retrieving these objects is to GET the Actor and look
# at the ID in the field for that object, then GET that ID. In practice
# Hathi will probably always use certain IDs for these objects, *HOWEVER* one
# should not assume that this will always be true.
#
# Here we are confirming that the Note we just created has been posted to the
# correct Outbox, and then propagated to the correct Inbox.
#
# Note that depending on have many items there are the "items" field may be
# a list or scalar.

getOutbox = {"testName": "Get outbox", "path": vars.usePath("act1Out"),
             "action": "GET", "data": None, "returnCode": 200,
             "returnData": {"@type": "OrderedCollection",
                            "@id": vars.useID("act1Out"),
                            "items": [vars.useID("createNote1")]}}

getInbox = {"testName": "Get inbox", "path": vars.usePath("act2In"),
            "action": "GET", "data": None, "returnCode": 200,
            "returnData": {"@type": "OrderedCollection",
                           "@id": vars.useID("act2In"),
                           "items": [vars.useID("createNote1")]}}


# Getting the Note
# ----------------
# Getting the Note that was previously posted shows that the content and
# summary fields have been turned into language-maps. And unlike the outbox
# or inbox, the Note has a simple numeric ID. This is true for nearly all
# objects in hathi; eventually the ID will be hashed, but for now it is an
# integer.
#
# Also note that the Note's "to" field is occupied, even though it was the
# Create Activity that had the address and not the Note. When creating an
# object the object's to/bto/cc/bcc/audience fields are syncronized with those
# in the Create Activity.

getCreate = {"testName": "Get note Create", "action": "GET", "data": None,
             "path": vars.usePath("createNote1"), "returnCode": 200,
             "returnData": {"@type": "Create", "@id": vars.useID("createNote1"),
                            "to": [vars.useID("act2")], "bto": [], "cc": [],
                            "bcc": [], "audience": [],
                            "object": {'@id': vars.newVar("note1"),
                                       '@type': 'Note',
                                       'attributedTo': vars.useID("act1"),
                                       'content': 'blah blah',
                                       'summary': 'foo!',
                                       "to": [vars.useID("act2")],
                                       "bto": [], "cc": [], "bcc": [],
                                       "audience": []},
                            "origin": vars.useID("act1"),
                            "attributedTo": vars.useID("act1")}}

getNote = {"testName": "Get Note", "path": vars.usePath("note1"),
           "action": "GET", "data": None, "returnCode": 200,
           "returnData": {"@type": "Note", "@id": vars.useID("note1"),
                          "to": [vars.useID("act2")],
                          "content": "blah blah",
                          "summary": "foo!",
                          "attributedTo": "http://localhost:8080/testing", 
                          "audience": [], "bcc": [], "bto": [], "cc": []}}

# Deleting the Note
# -----------------
# Deleting an object is done by posting a Delete Activity with the target
# object's ID in the "object" field. If authorized, the server will replace
# the original object with a Tombstone and inform both the recipients of the
# original object, and the recipients specified in the Delete of the change.

deleteNote = {"testName": "Delete Note", "path": vars.usePath("act1Out"),
              "action": "POST",
              "data": {"@type": "Delete", "object": vars.useID("note1")},
              "returnCode": 201, "returnData": b"",
              "returnLocation": vars.newVar("delete1")}

# Getting the Tombstone
# ---------------------
# The important fields of the Tombstone are "formerType" and "deleted".
# 
# * formerType is the type of the object that was deleted.
# * deleted is the RFC3339 server timestamp of when the deletion occured
#
# Note that in this test "deleted" == None. That is because we have no idea
# what the timestamp will be, so the comparison function merely checks
# that the field exists and moves on.

getTomb = {"testName": "Get Tombstone", "path": vars.usePath("note1"),
           "action": "GET", "data": None, "returnCode": 200,
           "returnData": {"@type": "Tombstone", "@id": vars.useID("note1"),
                          "to": [vars.useID("act2")],
                          "audience": [], "bcc": [], "bto": [], "cc": [],
                          "formerType": "Note", "deleted": None}}

# Checking the propagation of the Delete

getOutbox2 = {"testName": "Check that Delete is in Outbox",
              "path": vars.usePath("act1Out"),
              "action": "GET", "data": None, "returnCode": 200,
              "returnData": {"@type": "OrderedCollection",
                             "@id": vars.useID("act1Out"),
                             "items": [vars.useID("createNote1"),
                                       vars.useID("delete1")]}}

getInbox2 = {"testName": "Check Delete Inbox propagation",
             "path": vars.usePath("act2In"),
             "action": "GET", "data": None, "returnCode": 200,
             "returnData": {"@type": "OrderedCollection",
                            "@id": vars.useID("act2In"),
                            "items": [vars.useID("createNote1"),
                                      vars.useID("delete1")]}}

# Following
# ---------
# A Follow request works about how one would expect: send a request to Follow
# a certain Actor and they can choose to accept or reject that request.
#
# Accepting or rejecting is done through the Accept, Reject, TentativeAccept,
# and TentativeReject Activities. The relevant Actors will be added to the
# relevant Following/Followers collections when the followed Actor sends the
# acceptance.

followRequest = {"testName": "Send Follow request",
                 "path": vars.usePath("act1Out"),
                 "action": "POST", "returnCode": 201,
                 "data": {"@type": "Follow",
                          "object": vars.useID("act2")},
                 "returnData": b"", "returnLocation": vars.newVar("follow1")}

# Acceptance
# ----------
# Here we are testing the "Accept" type, for a follow request. The initial
# implementation of Accept/TentativeAccept and their rejection counterparts
# will not have any code differences between the types. There will only be the
# semantic difference that the user sees.
#
# Note that the implementation of the Accept activity is unusually complex
# as it is actually the final step of several other commands, all of which do
# different things to their targets.

followAccept = {"testName": "Accept Follow request", "action": "POST",
                "path": vars.usePath("act2Out"), "returnCode": 201,
                "data": {"@type": "Accept", "object": vars.useID("follow1")},
                "returnData": b"", "returnLocation": vars.newVar("accept1")}


# Rejection of Follow
# -------------------
rejectionRequest = {"testName": "Send Follow request for Rejection",
                    "path": vars.usePath("act2Out"), "action": "POST",
                    "returnCode": 201,
                    "data": {"@type": "Follow",
                             "object": vars.useID("act1")},
                    "returnData": b"",
                    "returnLocation": vars.newVar("fol-rej1")}

rejectionComplete = {"testName": "Reject Follow request", "action": "POST",
                     "path": vars.usePath("act1Out"), "returnCode": 201,
                     "data": {"@type": "Reject",
                              "object": vars.useID("fol-rej1")},
                     "returnData": b"",
                     "returnLocation": vars.newVar("reject1")}

# Completed Follow
# ----------------
# Ok, a follow has been sent and accepted. Now we need to make sure everyone
# has been addd to the proper collections.

checkFollowers = {"testName": "Check Followers collection", "action": "GET",
                  "path": vars.usePath("act2Folw"), "returnCode": 200,
                  "data": None,
                  "returnData": {"@type": "OrderedCollection",
                                 "@id": vars.useID("act2Folw"),
                                 "items": [vars.useID("act1")]}}

checkFollowing = {"testName": "Check Following collection", "action": "GET",
                  "path": vars.usePath("act1Folg"), "returnCode": 200,
                  "data": None,
                  "returnData": {"@type": "OrderedCollection",
                                 "@id": vars.useID("act1Folg"),
                                 "items": [vars.useID("act2")]}}

# Follow with TentativeAccept
# ---------------------------
# Most of this was explained in the previous sequence. Remember that the only
# difference with a TentativeAccept is what is conveys to the user.

followRequestTent = {"testName": "Send Follow request",
                     "path": vars.usePath("act2Out"),
                     "action": "POST", "returnCode": 201,
                     "data": {"@type": "Follow",
                              "object": vars.useID("act1")},
                     "returnData": b"",
                     "returnLocation": vars.newVar("follow2")}

followAcceptTent = {"testName": "Accept Follow request", "action": "POST",
                    "path": vars.usePath("act1Out"), "returnCode": 201,
                    "data": {"@type": "TentativeAccept",
                             "object": vars.useID("follow2")},
                    "returnData": b"", "returnLocation": vars.newVar("accept2")}

checkFollowers2 = {"testName": "Check Followers collection", "action": "GET",
                   "path": vars.usePath("act1Folw"), "returnCode": 200,
                   "data": None,
                   "returnData": {"@type": "OrderedCollection",
                                  "@id": vars.useID("act1Folw"),
                                  "items": [vars.useID("act2")]}}

checkFollowing2 = {"testName": "Check Following collection", "action": "GET",
                   "path": vars.usePath("act2Folg"), "returnCode": 200,
                   "data": None,
                   "returnData": {"@type": "OrderedCollection",
                                  "@id": vars.useID("act2Folg"),
                                  "items": [vars.useID("act1")]}}


# Test Reply functionality
# ------------------------

createRepliee = {"testName": "Create Note to reply to", "action": "POST",
                 "returnCode": 201, "path": vars.usePath("act1Out"),
                 "data": {"@type": "Create",
                          "object": {"@type": "Note", "content": "Foo bar!"}},
                 "returnLocation": vars.newVar("replyOneCreate"),
                 "returnData": b""}

getReplieeCreate = {"testName": "Get repliee-note creation", "action": "GET",
                    "returnCode": 200, "path": vars.usePath("replyOneCreate"),
                    "data": None,
                    "returnData": {
                        "@type": "Create",
                        "object": {
                            "@id": vars.newVar("replyOne"), 
                            "@type": "Note", 
                            "attributedTo": vars.useID("act1"), 
                            "audience": [], "bcc": [], "bto": [], "cc": [],
                            "content": "Foo bar!", "to": []},
                        "@id": vars.useID("replyOneCreate"),
                        "origin": vars.useID("act1"),
                        "attributedTo": vars.useID("act1"),
                        "audience": [], "bcc": [], "bto": [], "cc": [],
                        "to": []}}

createReplier = {"testName": "Create reply Note", "action": "POST",
                 "returnCode": 201, "path": vars.usePath("act2Out"),
                 "data": {"@type": "Create",
                          "object": {"@type": "Note", "content":"Bar foo!",
                                     "inReplyTo": vars.useID("replyOne")}},
                 "returnLocation": vars.newVar("replyTwoCreate"),
                 "returnData": b""}

getReplierCreate = {"testName": "Get replier-note creation", "action": "GET",
                    "returnCode": 200, "path": vars.usePath("replyTwoCreate"),
                    "data": None,
                    "returnData": {
                        "@type": "Create",
                        "origin": vars.useID("act2"),
                        "@id": vars.useID("replyTwoCreate"),
                        "object": {
                            "@id": vars.newVar("replyTwo"),
                            "@type": "Note",
                            "attributedTo": vars.useID("act2"),
                            "audience": [], "bcc": [], "bto": [], "cc": [],
                            "content": "Bar foo!", "to": [],
                            "inReplyTo": vars.useID("replyOne")},
                        "attributedTo": vars.useID("act2"),
                        "audience": [], "bcc": [], "bto": [], "cc": [],
                        "to": []}}

getReplieePost = {"testName": "Get repliee-note after reply", "action": "GET",
                  "returnCode": 200, "path": vars.usePath("replyOne"),
                  "data": None,
                  "returnData": {"@type": "Note",
                                 "content": "Foo bar!",
                                 "@id": vars.useID("replyOne"),
                                 "replies": vars.newVar("repliesCol"),
                                 "audience": [], "bcc": [], "bto": [],
                                 "cc": [], "to": [],
                                 "attributedTo": vars.useID("act1")}}

getRepliesCol = {"testName": "Get replies collection", "action": "GET",
                 "returnCode": 200, "path": vars.usePath("repliesCol"),
                 "data": None,
                 "returnData": {"@type": "Collection",
                                "items": [vars.useID("replyTwo")],
                                "@id": vars.useID("repliesCol")}}

# ==========================================================================
# This tuple contains the set of transactions between the client and server.

c2s_sequences = (createActor, createActor2, getActor1, getActor2, createSimple,
                 getOutbox, getInbox, getCreate, getNote, deleteNote, getTomb,
                 getOutbox2, getInbox2, followRequest, followAccept,
                 rejectionRequest, rejectionComplete, checkFollowers,
                 checkFollowing, followRequestTent, followAcceptTent,
                 checkFollowing2, checkFollowers2, createRepliee,
                 getReplieeCreate, createReplier, getReplierCreate,
                 getReplieePost, getRepliesCol)
