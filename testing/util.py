#!/usr/bin/env python3
# -*- coding: utf-8 -*-

APCoreFields = ["subject", "relationship", "actor", "attributedTo",
                "attachment", "attachments", "author", "bcc", "bto", "cc",
                "context", "current", "first", "generator", "icon", "image",
                "inReplyTo", "items", "instrument", "orderedItems", "last",
                "location", "next", "object", "oneOf", "anyOf", "origin",
                "accuracy", "prev", "preview", "provider", "replies", "result",
                "scope", "partOf", "tag", "target", "to", "url", "alias",
                "altitude", "content", "contentMap", "displayName",
                "displayNameMap", "downstreamDuplicates", "duration",
                "durationIso", "endTime", "height", "href", "hreflang", "id"
                "latitude", "longitude", "mediaType", "objectType", "priority",
                "published", "radius", "rating", "rel", "startIndex",
                "startTime", "summary", "summaryMap", "title", "titleMap",
                "totalItems", "units", "updated", "upstreamDuplicates", "verb",
                "width", "describes"]

def makeIDFromPath(host, port, path):
    path = path.lstrip("/")
    return "http://%s:%i/%s" % (host, port, path)
