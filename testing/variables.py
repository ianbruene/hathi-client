#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import urllib.parse

class VariableNode:
    def __init__(self, mode, variableName, database):
        self.mode = mode
        self.varName = variableName
        self.core = database

    def isSet(self):
        return self.mode == "set"

    def set(self, value):
        #print("Set:", self.varName, value, end="")
        if self.mode == "set":
            #print(" ...done")
            self.core.set(self.varName, value)
        else:
            #print(" ...BAD")
            raise TypeError("Incorrect set of %s" % self.varName)

    def get(self):
        #print("Get:", self.varName, end="")
        if self.mode == "get":
            tmp = self.core.getID(self.varName)
            #print(" get =", tmp)
            return tmp
        elif self.mode == "path":
            tmp = self.core.getPath(self.varName)
            #print(" path =", tmp)
            return tmp
        else:
            #print(" ...BAD")
            raise TypeError("Incorrect get of %s" % self.varName)

    def __repr__(self):
        s = "VN(%s, %s, %s)"
        data = None if self.mode == "set" else self.core.getID(self.varName)
        return s % (self.mode, self.varName, data)

class VariableDatabase:
    def __init__(self, defaultHost=None, defaultPort=8080):
        self.host = defaultHost
        self.port = defaultPort
        self.variables = {}
        self.remoteServers = {}  # Index same as variables

    def setServerData(self, hostname, port):
        #print("Setting Server Data", hostname, port)
        self.host = hostname
        self.port = port

    def makeID(self, localid):
        return "http://%s:%s/%s" % (self.host, self.port, localid)

    def newVar(self, variableName):
        node = VariableNode("set", variableName, self)
        return node

    def useID(self, variableName):
        node = VariableNode("get", variableName, self)
        return node

    def usePath(self, variableName):
        node = VariableNode("path", variableName, self)
        return node

    def newID(self, variableName, localid):
        # Used when we expect a specific ID. A constant string won't work
        # because of hostname and port issues
        self.variables[variableName] = localid
        node = VariableNode("get", variableName, self)
        return node

    def newIDPath(self, variableName, localid):
        # Same as newID except used where usePath would be used
        self.variables[variableName] = localid
        node = VariableNode("path", variableName, self)
        return node

    def set(self, variableName, value):
        path = urllib.parse.urlparse(value).path
        self.variables[variableName] = path.lstrip("/")

    def getID(self, variableName):
        # We let the KeyError happen as our error
        return self.makeID(self.variables[variableName])

    def getPath(self, variableName):
        return "/" + (self.variables[variableName].lstrip("/"))
